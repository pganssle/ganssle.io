---
title: Contact
layout: page
permalink: /contact/
---

You can e-mail me at paul (at) ganssle (dot) io.

I'm pretty friendly and don't mind unsolicited e-mails from actual people, but I'm not interested in recruiting offers or any other sort of bulk or commercial e-mails.
