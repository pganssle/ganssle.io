---
title: Talks
layout: page
permalink: /talks/
---

# Talks

### pytest for unittesters {% include anchor_tag.html tag="pytest-for-unittesters-pycon" %}
 (2024-05-17) Given at [PyCon 2024](https://us.pycon.org/2024/), this is a 30-minute talk designed to tout the benefits of `pytest` to people who currently use `unittest`.

 - [Abstract](https://us.pycon.org/2024/schedule/presentation/90/)
 - [Slides](https://pganssle-talks.github.io/2024-pycon-us-pytest/#/)
 - [Repo](https://github.com/pganssle-talks/2024-pycon-us-pytest)

### Using a Progressive Web App to Teach My Son Absolute Pitch {% include anchor_tag.html tag="perfect-pitch-nerd" %}
 (2024-03-09) Given at [NERD Summit 2024](https://nerdsummit.org/), this is a 45-minute talk about how and why I built the [Chord Identification Method Trainer](https://pganssle.github.io/cim) application, which can be used to train 2-6 year olds to have perfect pitch.

 - [Video](https://youtu.be/l2Z6uEsx9lE)
 - [Slides](https://pganssle-talks.github.io/2024-nerd-summit-chord-trainer/#/)
 - [Repo](https://github.com/pganssle-talks/2024-nerd-summit-chord-trainer)

### Working with Time Zones: Everything You Wish You Didn't Need to Know (`zoneinfo` edition) {% include anchor_tag.html tag="working-with-timezones-zoneinfo-pycon" %}
 (2023-04-21) Given at [PyCon US 2023](https://us.pycon.org/2023), this is a 30-minute talk on working with time zones. It is an update of *[Working with Time Zones](#working-with-timezones-pycon)* and *[`zoneinfo`: A stunning module of exceptional quality](#zoneinfo-chipy)*.

 - [Video](https://youtu.be/rYzgroaK8_Q)
 - [Abstract](https://us.pycon.org/2023/schedule/presentation/51/)
 - [Slides](https://pganssle-talks.github.io/pycon-us-2023-timezones)
 - [Repo](https://github.com/pganssle-talks/pycon-us-2023-timezones)

### What to Do When the Bug is in Someone Else's Code {% include anchor_tag.html tag="upstream-bugs" %}

(2022-04-29) Given at [PyCon US 2022](https://us.pycon.org/2022/), this is a 30 minute talk covering several strategies for dealing with upstream bugs — whether they are in open source libraries or owned by another team.

 - [Video](https://youtu.be/LTMguK-XJEo)
 - [Abstract](https://us.pycon.org/2022/schedule/presentation/86/)
 - [Slides](https://pganssle-talks.github.io/pycon-us-2022-upstream-bugs/) {% include pdf.html url="../files/talks/2022-04-29-pycon_us_2022_upstream_bugs.pdf" %}
 - [Repo](https://github.com/pganssle-talks/pycon-us-2022-upstream-bugs)


(2020-10-24) Given at [PyTexas 2020](https://pytexas.org), this is a 30 minute talk covering several strategies for dealing with upstream bugs — whether they are in open source libraries or owned by another team.

 - [Video](https://youtu.be/S4TjOnkFLtI)
 - [Slides](https://pganssle-talks.github.io/pytexas-2020-upstream-bugs)
 - [Repo](https://github.com/pganssle-talks/pytexas-2020-upstream-bugs)


### `xfail` and `skip`: What to do with tests you know will fail {% include anchor_tag.html tag="xfail-and-skip" %}

(2022-03-26) Given at [PyTexas 2022](https://www.pytexas.org), this talk covers skipping tests or marking them as expected failures. It grew out of [a lightning talk on the same subject](#xfail-lightning), and the accompanying blog post ["How and why I use `pytest`'s `xfail`](https://blog.ganssle.io/articles/2021/11/pytest-xfail.html).

 - [Video](https://youtu.be/9pXGZdmQIUQ)
 - [Slides](https://pganssle-talks.github.io/pytexas-2022-xfail/#/) · {% include pdf.html url="../files/talks/2022-03-26_pytexas_2022_xfail.pdf" %}
 - [Repo](https://github.com/pganssle-talks/pytexas-2022-xfail)
 - [Abstract](https://web.archive.org/web/20220316210510/https://www.pytexas.org/schedule/talks/#xfail)

### The Stable Interface Paradox {% include anchor_tag.html tag="stable-interface-paradox" %}

(2020-12-05) Given at [PyConf Hyderabad 2020](https://pyconf.hydpy.org/2020/), this is a 1-hour keynote talk. It introduce what I call the "stable interface paradox" — the unfortunate catch-22 that the fewer users of your interface you have, the less information you have about the right way to design the interface, but the more users you have, the harder it is to actually change that interface.

 - [Video](https://youtu.be/aRqulQUgiIA)
 - [Slides](https://pganssle-talks.github.io/pyconf-hyderabad-2020-stable-interface)
 - [Repo](https://github.com/pganssle-talks/pyconf-hyderabad-2020-stable-interface)

### `zoneinfo`: A stunning module of exceptional quality {% include anchor_tag.html tag="zoneinfo-chipy" %}

(2020-11-12) Given at the [ChiPy `__main__` meetup](https://www.meetup.com/_ChiPy_/events/274038377/) for November 2020. This is a significant update of <a href="#working-with-timezones-pycon"><i>Working with Time Zones</i></a>, with additional history on the development of the `zoneinfo` module.

 - [Video](https://youtu.be/8JFUgAJLoQE)
 - [Slides](https://pganssle-talks.github.io/chipy-nov-2020-zoneinfo)
 - [Repo](https://github.com/pganssle-talks/chipy-nov-2020-zoneinfo)
 - [Abstract](https://www.meetup.com/_ChiPy_/events/274038377/)

### Build Your Python Extensions in Rust! {% include anchor_tag.html tag="python-backend-rust" %}
(2019-10-05) Given at [PyGotham 2019](https://2019.pygotham.org/), this is a 25 minute talk on writing Rust backends for Python modules.
 - [Video](https://pyvideo.org/pygotham-2019/build-your-python-extensions-with-rust.html)
 - [Abstract](https://2019.pygotham.org/talks/build-your-python-extensions-with-rust/)
 - [Slides](https://pganssle-talks.github.io/pygotham-2019-rust-extensions)
 - [Repo](https://github.com/pganssle-talks/pygotham-2019-rust-extensions)

 (2019-07-10) Given at [EuroPython 2019](https://ep2019.europython.eu/), this is a 30 minute talk on writing Rust backends for Python modules.
 - [Video](https://youtu.be/FolV-xUD3Ko)
 - [Abstract](https://ep2019.europython.eu/talks/VJsGhga-build-your-python-extensions-with-rust/)
 - [Slides](https://pganssle-talks.github.io/europython-2019-rust-extensions/#/)
 - [Repo](https://github.com/pganssle-talks/europython-2019-rust-extensions)

 (2019-06-16) Given at [PyLondinium 2019](https://pylondinium.org), this is a 25 minute talk on writing Rust backends for Python modules.
 - [Video](https://youtu.be/IqQDl_2BsNs)
 - [Abstract](https://pylondinium.org/talks/talk-16.html)
 - [Slides](https://pganssle-talks.github.io/pylondinium-2019-rust-extensions/#/)
 - [Repo](https://github.com/pganssle-talks/pylondinium-2019-rust-extensions)

### Gathering Related Functionality: Patterns for Cleaner API Design  {% include anchor_tag.html tag="gathering-related-func" %} 
<a name="gathering-related-func-pybay"/> (2019-08-16) *Given as: Patterns for Clean API Design*. Given at [PyBay 2019](https://web.archive.org/web/20190910130443/https://pybay.com/), this grew out of my <a href="#variants-lighting">`variants`</a> lighting talk. This talk sets out some general design principles for library interfaces and explores ways to mark relatedness in your APIs while keeping a clean top-level interface. (30 minutes)
 - [Video](https://youtu.be/9rKx-8bj-R0)
 - [Abstract](https://web.archive.org/web/20210727123530/https://pybay.com/speaker/paul-ganssle/)
 - [Slides](https://pganssle-talks.github.io/pybay-2019-clean-apis)
 - [Repo](https://github.com/pganssle-talks/pybay-2019-clean-apis)

<a name="gathering-related-func-ca"/>(2018-11-10) Given at [PyCon CA 2018](https://2018.pycon.ca), this grew out of my <a href="#variants-lighting">`variants`</a> lighting talk. This talk sets out some general design principles for library interfaces and explores ways to mark relatedness in your APIs while keeping a clean top-level interface. (30 minutes)
 - [Video](https://youtu.be/s8Rn188mNW4)
 - [Abstract](https://2018.pycon.ca/talks/talk-PC-52797/)
 - [Slides](https://pganssle-talks.github.io/pycon-ca-2018-clean-apis)
 - [Repo](https://github.com/pganssle-talks/pycon-ca-2018-clean-apis)

 (2018-10-06) Given at [PyGotham 2018](https://2018.pygotham.org), this grew out of my <a href="#variants-lighting">`variants`</a> lighting talk. This talk sets out some general design principles for library interfaces and explores ways to mark relatedness in your APIs while keeping a clean top-level interface. (20 minutes)
 - [Video](https://youtu.be/nmoT-E8fi_8)
 - [Abstract](https://2018.pygotham.org/talks/gathering-related-functionality-patterns-for-clean-api-design/)
 - [Slides](https://pganssle-talks.github.io/pygotham-2018-clean-apis/#/)
 - [Repo](https://github.com/pganssle-talks/pygotham-2018-clean-apis)

### Working with Time Zones: Everything You Wish You Didn't Need to Know {% include anchor_tag.html tag="working-with-timezones-pycon" %}
 (2019-05-05) Given at [PyCon US 2019](https://us.pycon.org/2019), this is a 25-minute talk on working with time zones.
 - [Video](https://youtu.be/rz3D8VG_2TY)
 - [Abstract](https://us.pycon.org/2019/schedule/presentation/191/)
 - [Slides](https://pganssle-talks.github.io/pycon-us-2019-timezones)
 - [Repo](https://github.com/pganssle-talks/pycon-us-2019-timezones)

 (2018-06-10) Given at [PyLondinium](https://pylondinium.org), this is a shorter version of <a href="#timezone-troubles">Time Zone Troubles</a>. (20 minute version)
 - [Video](https://youtu.be/TPrmyAZZPi0)
 - [Abstract](https://pylondinium.org/talk.html?talk_id=23)
 - [Slides](https://pganssle.github.io/pylondinium-2018-timezones-talk)
 - [Repo](https://github.com/pganssle/pylondinium-2018-timezones-talk)


### Contributing to Open Source: A Guide {% include anchor_tag.html tag="contributing-oss-pydata2018" %}
 (2018-10-17) Given at [PyData NYC 2018](https://pydata.org/nyc2018/), this is a beginner's guide to the social (and some technical) aspects of contributing to open source projects.
 - [Video](https://youtu.be/JhPC6_rO08s)
 - [Abstract](https://pydata.org/nyc2018/schedule/presentation/26/)
 - [Slides](https://pganssle-talks.github.io/pydata-nyc-2018-open-source)
 - [Repo](https://github.com/pganssle-talks/pydata-nyc-2018-open-source)

### Time Zone Troubles: Dealing with Imaginary and Ambiguous Datetimes {% include anchor_tag.html tag="timezone-troubles" %}
 (2017-08-12) Given at [PyBay 2017](http://pybay.com), this is an in-depth look at time zones in Python. (40 minute version)
 - [Video](https://www.youtube.com/watch?v=l4UCKCo9FWY)
 - [Slides](https://pganssle.github.io/pybay-2017-timezones-talk)
 - [Repo](https://github.com/pganssle/pybay-2017-timezones-talk)

### python-dateutil: A delightful romp in the never-confusing world of dates and times {% include anchor_tag.html tag="python-dateutil-talk" %}

(2018-06-01) Given at Taiwanese Data Professionals event "[It's About Time, Data People!"](https://www.eventbrite.com/e/its-about-time-data-people-tickets-45977098740) (20 minute version)

- [Video](https://youtu.be/EcG6Mg-4fHE?t=25m5s)
- [Slides](https://pganssle.github.io/tdp-2018-dateutil-talk)
- [Repo](https://github.com/pganssle/tdp-2018-dateutil-talk)

(2016-07-17) Given at [PyGotham](http://2016.pygotham.org/), this is an overview of the `dateutil` package. Audio is quite choppy.

- [Video](https://www.youtube.com/watch?v=X_bCto95Imc)
- [Slides](https://pganssle.github.io/pygotham-2016-dateutil-talk/)
- [Repo](https://github.com/pganssle/pygotham-2016-dateutil-talk)

------

# Tutorials

### Dealing with Datetimes {% include anchor_tag.html tag="dealing-with-datetimes-tutorial" %}
(2019-05-01) Given at [PyCon US 2019](https://us.pycon.org/2019/), this is a tutorial that covers time zones, serialization and recurring events. The materials can be run in a self-paced manner, and there is much more material than there was time to cover in the tutorial.
- [Video](https://youtu.be/Xt8age1Be4E)
- [Abstract](https://us.pycon.org/2019/schedule/presentation/191/)
- [Slides](https://pganssle-talks.github.io/pycon-us-2019-timezones)
- [Repo](https://github.com/pganssle-talks/pycon-us-2019-timezones)


------

# Lightning Talks

### Documentation Exercises {% include anchor_tag.html tag="doc-exercises-lightning" %}
Introduces the concept of documentation exercises: exercises both in and *for* your documentation.
- [Slides](https://pganssle-talks.github.io/doc-exercises-lightning) · {% include pdf.html url="../files/talks/2022-03-27-pytexas_2022_doc_exercises.pdf" %}
- [Repo](https://github.com/pganssle-talks/doc-exercises-lightning)

### Test your Failures with xfail {% include anchor_tag.html tag="xfail-lightning" %}
A talk on marking tests as expected to fail, and why that's a good idea.
- [Video](https://youtu.be/YmAXhlcNbys?t=512) (PyGotham 2019)
- [Slides](https://pganssle-talks.github.io/xfail-lightning)
- [Repo](https://github.com/pganssle-talks/xfail-lightning)

### variants: A convention for cleaner API design {% include anchor_tag.html tag="variants-lightning" %}
A talk about the [variants](https://github.com/python-variants/variants) library I wrote for Python.
- [Video](https://www.youtube.com/watch?v=tzFWz5fiVKU&t=952s) (PyCon 2018)
- [Slides](https://pganssle.github.io/lightning-talks/python-variants)
- [Repo](https://github.com/pganssle/lightning-talks/tree/master/python-variants)

### GridStrategy: A Proposed Mechanism for Automatic Subplot Management {% include anchor_tag.html tag="gridstrategy-lightning" %}
A talk about my proposal ([MEP 30](https://github.com/matplotlib/matplotlib/wiki/MEP30%3A-Grid-population-strategies)) for a new somewhat automagical subplot manager for matplotlib

- [Slides](https://pganssle.github.io/lightning-talks/grid-strategy/)
- [Repo](https://github.com/pganssle/lightning-talks/tree/master/grid-strategy)

### Time Zone Tools
 One of the earliest forms of my talk on time zones, given at PyCon 2017.
 - [Video](https://youtu.be/SXl-pZnoaQ0?t=1228)
