---
title: Projects
layout: page
permalink: /projects/
---

# Projects

### python-dateutil (maintainer)
Popular python datetime library with support for datetime string parsing, time zones, recurrence rules and more.

- [Github](https://github.com/dateutil/dateutil)
- [Docs](https://dateutil.readthedocs.io)

### variants
Syntactic sugar for creating variant forms of Python functions

- [Github](https://github.com/python-variants/variants)
- [Docs](https://variants.readthedocs.io)

### Hapticap
Haptic compass built into a hat

- [Website](https://pganssle.github.io/HaptiCap/)

### audio-feeder
Audiobook server that serves your audiobooks as podcast feeds.
- [Github](https://github.com/pganssle/audio-feeder)
