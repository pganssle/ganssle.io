---
title: About
layout: page
permalink: /about/
---

Paul Ganssle is a software developer at Google and contributor to various open source projects. Among other projects, he is a core developer of the Python language and (poorly) maintains [python-dateutil](https://dateutil.readthedocs.io).

He previously was a physical chemist working on low-field NMR. If you'd like, you can read his dissertation [Alkali Vapor-Cell Magnetometry and its Application to Relaxometry and Diffusometry](https://github.com/pganssle-research/alkali-vapor-cell-magnetometry-dissertation) ([direct PDF link](https://github.com/pganssle-research/alkali-vapor-cell-magnetometry-dissertation/raw/master/Paul%20Ganssle%20-%20Alkali%20Vapor-Cell%20Magnetometry%20and%20its%20Application%20to%20Low-Field%20Relaxometry%20and%20Diffusometry.pdf)), though he is well aware that "I would like to read his dissertation" is probably the last thing on anyone's mind.

All opinions expressed on this site are solely those of the author and do not necessarily reflect those of his employer.
