---
title: Curriculum Vitae
layout: page
extra_css: ["cv.css"]
permalink: /cv/
---
# Curriculum Vitae

<ul class="toc">
    <li class="toc_elem"><a href="#education">Education</a></li>
    <li class="toc_elem"><a href="#work_experience">Work Experience</a></li>
    <li class="toc_elem"><a href="#open_source">Open Source Experience</a></li>
    <li class="toc_elem"><a href="#teaching_experience">Teaching Experience</a></li>
    <li class="toc_elem"><a href="#publications">Publications</a></li>
    <li class="toc_elem"><a href="#presentations">Presentations</a></li>
    <li class="toc_elem"><a href="#advanced_coursework">Advanced Elective Coursework</a></li>
</ul>

## Education {% include anchor_tag.html tag="education" %}
<ul class="education">
{% for entry in site.data.cv_contents.education %}
<li>
<div class="education_entry">
    <span id="degree">{{ entry.degree }}</span>, <span id="institution">{{ entry.institution }}</span> <span id="date">{{ entry.date }}</span>
    {% if entry.thesis %}<br/>
    Thesis: <span id="thesis"><a href="{{ entry.thesis.link }}">{{ entry.thesis.text }}</a></span>
    {% endif %}
</div>
</li>
{% endfor %}
</ul>


## Work Experience {% include anchor_tag.html tag="work_experience" %}
<div class="work_experience">
{% for entry in site.data.cv_contents.work_experience %}
<div class="work_entry">
<span id="title">{{ entry.title }}</span>, {%if entry.department %}<span id="department">{{ entry.department }}</span>, {% endif %}<span id="company">{{ entry.company }}</span><br/>
    <span id="dates">{{ entry.date_start | date: '%b %Y'}} - {{ entry.date_end | date: '%b %Y' | default: "Present" }}</span><br/>
    {% if entry.advisor %}
    <span id="advisor-key">Advisor:</span> <span id="advisor">{{ entry.advisor }}</span>
    {% endif %}
    {% if entry.details %}
    <ul class="work_details">
        {% for detail in entry.details %}
        <li class="work_details">{{ detail }}</li>
        {% endfor %}
    </ul>
    {% endif %}
</div>
{% endfor %}
</div>


## Open Source Experience {% include anchor_tag.html tag="open_source" %}
<div class="open_source">
{% for entry in site.data.cv_contents.open_source %}
<div class="oss_entry">
    <span id="role">{{ entry.role }}</span>, <span id="project">
    {% if entry.url %}
        <a href="{{ entry.url }}">{{ entry.project }}</a>
    {% else %}
        {{ entry.project }}
    {% endif %}
    </span><br/>
    {% if entry.date_start %}
        <span id="dates">{{ entry.date_start | date: '%b %Y'}} - {{ entry.date_end | date: '%b %Y' | default: "Present" }}</span><br/>
    {% endif %}
    <div id="description">{{ entry.description }}</div>
</div>
{% endfor %}
</div>


## Teaching Experience {% include anchor_tag.html tag="teaching_experience" %}
<div class="teaching">
{% for entry in site.data.cv_contents.teaching_experience %}
    <div class="teaching_entry">
    <span id="title">{{ entry.title }}</span>, <span id="institution">{{ entry.institution }}</span>
    <ul class="instances">
        {% for instance in entry.instances %}
            {% assign course = instance.course | default: entry.course %}
            {% assign semester = instance.semester | default: entry.semester %}
            {% assign instructor = instance.instructor | default: entry.instructor %}
            <li id="instance">
                <span id="course">{% if course.number %}{{ course.number }}: {% endif %}{{ course.name }}</span>, <span id="semester">{{ semester }}</span><br/>
                <span id="instructor"><span id="label">Instructor:</span> {{ instructor }}</span>
                </li>
        {% endfor %}
    </ul>
    {% if entry.details %}
        <ul class="details">
        {% for detail in entry.details %}
            <li id="detail">{{detail}}</li>
        {% endfor %}
        </ul>
    {% endif %}
    </div>
{% endfor %}
</div>

## Publications {% include anchor_tag.html tag="publications" %}
<div class="publications">
<ul class="publication_list primary_list">
{% for paper in site.data.cv_contents.publications %}
    <li id="publication">
        {% assign me="P. Ganssle" %}
        {% for author in paper.authors %}
        <span class="author" {% if author == me %}id="me" {% endif %}>{{author}}</span>, 
        {% endfor %}
        <span id="title">"{{ paper.title }}"</span>,
        <span id="journal">{{ paper.publication }}</span>,
        {% if paper.cover %}<span id="cover">
            {% if paper.cover_url %}
                <a href="{{ paper.cover_url }}">{{ paper.cover }}</a>
            {% else %}
                {{ paper.cover }}
            {% endif %}
            </span>,
        {% endif %}
        {% if paper.issue %}<span id="issue">{{ paper.issue }}</span>, {% endif %}
        {% if paper.year %}<span id="year">{{ paper.year }}</span>, {% endif %}
        {% if paper.pages %}<span id="pages">{{ paper.pages }}</span>{% endif %}
        {% if paper.doi %} <a href="https://doi.org/{{paper.doi}}">{{ paper.doi }}</a>{% endif %}
        {% if paper.pdf %}&middot; {% include pdf.html url=paper.pdf %}{% endif %}

    </li>
{% endfor %}
</ul>
</div>

## Presentations {% include anchor_tag.html tag="presentations" %}
<h3 id="talks" class="with_note">Talks {% include anchor_tag.html tag="talks" %}</h3>
<div class="talks">
<div class="note">See also <a href="../talks/">Talks</a></div>
<ul class="talk_list primary_list">
{% for talk in site.data.cv_contents.talks %}
    <li class="talk">
    {% if talk.instances %}
        <span id="talk_title">"{{ talk.title }}"</span>
        <ul class="sub_list talk_instance_list">
        {% for instance in talk.instances %}
            <li id="talk_instance">
                {% include render_talk_event.html event=instance %},
                <span id="talk_location">{{ location }}</span>{% if instance.date %},
                <span id="talk_date">{{ instance.date | date: "%Y-%m-%d" }}</span>{% endif %}
            </li>
        {% endfor %}
        </ul>
    {% else %}
        <span id="talk_title">"{{ talk.title }}"</span>, {% include render_talk_event.html event=talk %}, 
        <span id="talk_location">{{ location }}</span>{% if talk.date %},
        <span id="talk_date">{{ talk.date | date: "%Y-%m-%d" }}</span>{% endif %}
    {% endif %}
    </li>
{% endfor %}

</ul>
</div>

## Advanced Elective Coursework {% include anchor_tag.html tag="advanced_coursework" %}
<div class="advanced_coursework">
{% for entry in site.data.cv_contents.courses %}
<div class="institution">
<h4 id="university">{{ entry.institution }}</h4>
<ul class="course_entries">
    {% for course in entry.courses %}
    <li class="course_entry">
        <span id="number">{{ course.number }}</span>:
        <span id="name">{{ course.name }}</span>
        (<span id="instructor">{{ course.instructor }}</span>,
         <span id="semester">{{ course.semester }}</span>
         {% if course.audited %}, <span id="audited">Audited</span>{% endif %})
    </li>
    {% endfor %}
</ul>
</div>
{% endfor %}
</div>
